<?php

/**
 * @file
 * uw_ct_feds_clubs.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_ct_feds_clubs_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'feds_clubs';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Feds Clubs';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Clubs';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '12';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_type'] = 'h2';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Clubs categories */
  $handler->display->display_options['fields']['field_clubs_categories']['id'] = 'field_clubs_categories';
  $handler->display->display_options['fields']['field_clubs_categories']['table'] = 'field_data_field_clubs_categories';
  $handler->display->display_options['fields']['field_clubs_categories']['field'] = 'field_clubs_categories';
  $handler->display->display_options['fields']['field_clubs_categories']['label'] = 'Categories';
  $handler->display->display_options['fields']['field_clubs_categories']['element_type'] = 'span';
  $handler->display->display_options['fields']['field_clubs_categories']['element_label_type'] = 'strong';
  $handler->display->display_options['fields']['field_clubs_categories']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_clubs_categories']['element_wrapper_class'] = 'club-category';
  $handler->display->display_options['fields']['field_clubs_categories']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_clubs_categories']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_clubs_categories']['multi_type'] = 'ul';
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  /* Field: Content: Club Website */
  $handler->display->display_options['fields']['field_club_website']['id'] = 'field_club_website';
  $handler->display->display_options['fields']['field_club_website']['table'] = 'field_data_field_club_website';
  $handler->display->display_options['fields']['field_club_website']['field'] = 'field_club_website';
  $handler->display->display_options['fields']['field_club_website']['label'] = '';
  $handler->display->display_options['fields']['field_club_website']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_club_website']['alter']['text'] = '<i class="fas fa-desktop" aria-hidden="true">&nbsp;</i><span class="off-screen">Website</span>
<a href="[field_club_website-url]">[field_club_website-title]</a>';
  $handler->display->display_options['fields']['field_club_website']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_club_website']['element_wrapper_type'] = 'span';
  $handler->display->display_options['fields']['field_club_website']['element_wrapper_class'] = 'club-website';
  $handler->display->display_options['fields']['field_club_website']['click_sort_column'] = 'url';
  /* Field: Content: Club email */
  $handler->display->display_options['fields']['field_club_email']['id'] = 'field_club_email';
  $handler->display->display_options['fields']['field_club_email']['table'] = 'field_data_field_club_email';
  $handler->display->display_options['fields']['field_club_email']['field'] = 'field_club_email';
  $handler->display->display_options['fields']['field_club_email']['label'] = '';
  $handler->display->display_options['fields']['field_club_email']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_club_email']['alter']['text'] = '<i class="fas fa-envelope" aria-hidden="true">&nbsp;</i><span class="off-screen">Email</span>
<a href="[field_club_email-email]">[field_club_email]</a>';
  $handler->display->display_options['fields']['field_club_email']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_club_email']['element_wrapper_type'] = 'span';
  $handler->display->display_options['fields']['field_club_email']['element_wrapper_class'] = 'club-email';
  /* Field: Content: Other */
  $handler->display->display_options['fields']['field_other']['id'] = 'field_other';
  $handler->display->display_options['fields']['field_other']['table'] = 'field_data_field_other';
  $handler->display->display_options['fields']['field_other']['field'] = 'field_other';
  $handler->display->display_options['fields']['field_other']['label'] = '';
  $handler->display->display_options['fields']['field_other']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_other']['alter']['text'] = '<i class="fa fa-link" aria-hidden="true">&nbsp;</i><span class="off-screen">Other Social Media Resource</span>
<a href="[field_other-url]">[field_other-title]</a>
';
  $handler->display->display_options['fields']['field_other']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_other']['click_sort_column'] = 'url';
  /* Field: Content: Facebook */
  $handler->display->display_options['fields']['field_facebook']['id'] = 'field_facebook';
  $handler->display->display_options['fields']['field_facebook']['table'] = 'field_data_field_facebook';
  $handler->display->display_options['fields']['field_facebook']['field'] = 'field_facebook';
  $handler->display->display_options['fields']['field_facebook']['label'] = '';
  $handler->display->display_options['fields']['field_facebook']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_facebook']['alter']['text'] = '<i class="fab fa-facebook-f" aria-hidden="true">&nbsp;</i><span class="off-screen">Facebook</span>
<a href="[field_facebook-url]">[field_facebook-url]</a>';
  $handler->display->display_options['fields']['field_facebook']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_facebook']['element_wrapper_type'] = 'span';
  $handler->display->display_options['fields']['field_facebook']['element_wrapper_class'] = 'club-facebook';
  $handler->display->display_options['fields']['field_facebook']['click_sort_column'] = 'url';
  /* Field: Content: Twitter */
  $handler->display->display_options['fields']['field_twitter']['id'] = 'field_twitter';
  $handler->display->display_options['fields']['field_twitter']['table'] = 'field_data_field_twitter';
  $handler->display->display_options['fields']['field_twitter']['field'] = 'field_twitter';
  $handler->display->display_options['fields']['field_twitter']['label'] = '';
  $handler->display->display_options['fields']['field_twitter']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_twitter']['alter']['text'] = '<i class="fab fa-twitter" aria-hidden="true">&nbsp;</i><span class="off-screen">Twitter</span>
<a href="[field_twitter-url]">[field_twitter-title]</a>
';
  $handler->display->display_options['fields']['field_twitter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_twitter']['element_wrapper_type'] = 'span';
  $handler->display->display_options['fields']['field_twitter']['element_wrapper_class'] = 'club-twitter';
  $handler->display->display_options['fields']['field_twitter']['click_sort_column'] = 'url';
  /* Field: Content: YouTube */
  $handler->display->display_options['fields']['field_youtube']['id'] = 'field_youtube';
  $handler->display->display_options['fields']['field_youtube']['table'] = 'field_data_field_youtube';
  $handler->display->display_options['fields']['field_youtube']['field'] = 'field_youtube';
  $handler->display->display_options['fields']['field_youtube']['label'] = '';
  $handler->display->display_options['fields']['field_youtube']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_youtube']['alter']['text'] = '<i class="fab fa-youtube" aria-hidden="true">&nbsp;</i><span class="off-screen">YouTube</span>
<a href="[field_youtube-url]">[field_youtube-url]</a>';
  $handler->display->display_options['fields']['field_youtube']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_youtube']['element_wrapper_type'] = 'span';
  $handler->display->display_options['fields']['field_youtube']['element_wrapper_class'] = 'club-youtube';
  $handler->display->display_options['fields']['field_youtube']['click_sort_column'] = 'url';
  /* Field: Content: Instagram */
  $handler->display->display_options['fields']['field_instagram']['id'] = 'field_instagram';
  $handler->display->display_options['fields']['field_instagram']['table'] = 'field_data_field_instagram';
  $handler->display->display_options['fields']['field_instagram']['field'] = 'field_instagram';
  $handler->display->display_options['fields']['field_instagram']['label'] = '';
  $handler->display->display_options['fields']['field_instagram']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_instagram']['alter']['text'] = '<i class="fab fa-instagram" aria-hidden="true">&nbsp;</i><span class="off-screen">Instagram</span>
<a href="[field_instagram-url]">[field_instagram-url]</a>';
  $handler->display->display_options['fields']['field_instagram']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_instagram']['element_wrapper_type'] = 'span';
  $handler->display->display_options['fields']['field_instagram']['element_wrapper_class'] = 'club-instagram';
  $handler->display->display_options['fields']['field_instagram']['click_sort_column'] = 'url';
  /* Sort criterion: Content: Title */
  $handler->display->display_options['sorts']['title']['id'] = 'title';
  $handler->display->display_options['sorts']['title']['table'] = 'node';
  $handler->display->display_options['sorts']['title']['field'] = 'title';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'feds_club' => 'feds_club',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Clubs categories (field_clubs_categories) */
  $handler->display->display_options['filters']['field_clubs_categories_tid']['id'] = 'field_clubs_categories_tid';
  $handler->display->display_options['filters']['field_clubs_categories_tid']['table'] = 'field_data_field_clubs_categories';
  $handler->display->display_options['filters']['field_clubs_categories_tid']['field'] = 'field_clubs_categories_tid';
  $handler->display->display_options['filters']['field_clubs_categories_tid']['group'] = 1;
  $handler->display->display_options['filters']['field_clubs_categories_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_clubs_categories_tid']['expose']['operator_id'] = 'field_clubs_categories_tid_op';
  $handler->display->display_options['filters']['field_clubs_categories_tid']['expose']['label'] = 'Clubs categories';
  $handler->display->display_options['filters']['field_clubs_categories_tid']['expose']['operator'] = 'field_clubs_categories_tid_op';
  $handler->display->display_options['filters']['field_clubs_categories_tid']['expose']['identifier'] = 'field_clubs_categories_tid';
  $handler->display->display_options['filters']['field_clubs_categories_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    21 => 0,
    16 => 0,
    19 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    18 => 0,
    20 => 0,
    17 => 0,
  );
  $handler->display->display_options['filters']['field_clubs_categories_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_clubs_categories_tid']['vocabulary'] = 'feds_club_categories';
  /* Filter criterion: Content: Title (title_field) */
  $handler->display->display_options['filters']['title_field_value']['id'] = 'title_field_value';
  $handler->display->display_options['filters']['title_field_value']['table'] = 'field_data_title_field';
  $handler->display->display_options['filters']['title_field_value']['field'] = 'title_field_value';
  $handler->display->display_options['filters']['title_field_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['title_field_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title_field_value']['expose']['operator_id'] = 'title_field_value_op';
  $handler->display->display_options['filters']['title_field_value']['expose']['label'] = 'Club Title';
  $handler->display->display_options['filters']['title_field_value']['expose']['operator'] = 'title_field_value_op';
  $handler->display->display_options['filters']['title_field_value']['expose']['identifier'] = 'title_field_value';
  $handler->display->display_options['filters']['title_field_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    21 => 0,
    16 => 0,
    19 => 0,
    13 => 0,
    14 => 0,
    15 => 0,
    3 => 0,
    6 => 0,
    12 => 0,
    5 => 0,
    4 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    11 => 0,
    10 => 0,
    18 => 0,
    20 => 0,
    17 => 0,
  );

  /* Display: Clubs Listings */
  $handler = $view->new_display('page', 'Clubs Listings', 'page');
  $handler->display->display_options['path'] = 'clubs/listing';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Club Listing';
  $handler->display->display_options['menu']['weight'] = '20';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: Club Feed */
  $handler = $view->new_display('feed', 'Club Feed', 'feed_1');
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'rss';
  $handler->display->display_options['row_plugin'] = 'node_rss';
  $handler->display->display_options['row_options']['item_length'] = 'full';
  $handler->display->display_options['path'] = 'feds-clubs-feed';
  $translatables['feds_clubs'] = array(
    t('Master'),
    t('Clubs'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('Categories'),
    t('<i class="fas fa-desktop" aria-hidden="true">&nbsp;</i><span class="off-screen">Website</span>
<a href="[field_club_website-url]">[field_club_website-title]</a>'),
    t('<i class="fas fa-envelope" aria-hidden="true">&nbsp;</i><span class="off-screen">Email</span>
<a href="[field_club_email-email]">[field_club_email]</a>'),
    t('<i class="fa fa-link" aria-hidden="true">&nbsp;</i><span class="off-screen">Other Social Media Resource</span>
<a href="[field_other-url]">[field_other-title]</a>
'),
    t('<i class="fab fa-facebook-f" aria-hidden="true">&nbsp;</i><span class="off-screen">Facebook</span>
<a href="[field_facebook-url]">[field_facebook-url]</a>'),
    t('<i class="fab fa-twitter" aria-hidden="true">&nbsp;</i><span class="off-screen">Twitter</span>
<a href="[field_twitter-url]">[field_twitter-title]</a>
'),
    t('<i class="fab fa-youtube" aria-hidden="true">&nbsp;</i><span class="off-screen">YouTube</span>
<a href="[field_youtube-url]">[field_youtube-url]</a>'),
    t('<i class="fab fa-instagram" aria-hidden="true">&nbsp;</i><span class="off-screen">Instagram</span>
<a href="[field_instagram-url]">[field_instagram-url]</a>'),
    t('Clubs categories'),
    t('Club Title'),
    t('Clubs Listings'),
    t('Club Feed'),
  );
  $export['feds_clubs'] = $view;

  return $export;
}
