<?php

/**
 * @file
 * uw_ct_feds_clubs.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_feds_clubs_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create feds_club content'.
  $permissions['create feds_club content'] = array(
    'name' => 'create feds_club content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'define view for terms in feds_club_categories'.
  $permissions['define view for terms in feds_club_categories'] = array(
    'name' => 'define view for terms in feds_club_categories',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'tvi',
  );

  // Exported permission: 'define view for vocabulary feds_club_categories'.
  $permissions['define view for vocabulary feds_club_categories'] = array(
    'name' => 'define view for vocabulary feds_club_categories',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'tvi',
  );

  // Exported permission: 'delete any feds_club content'.
  $permissions['delete any feds_club content'] = array(
    'name' => 'delete any feds_club content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own feds_club content'.
  $permissions['delete own feds_club content'] = array(
    'name' => 'delete own feds_club content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in feds_club_categories'.
  $permissions['delete terms in feds_club_categories'] = array(
    'name' => 'delete terms in feds_club_categories',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any feds_club content'.
  $permissions['edit any feds_club content'] = array(
    'name' => 'edit any feds_club content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own feds_club content'.
  $permissions['edit own feds_club content'] = array(
    'name' => 'edit own feds_club content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in feds_club_categories'.
  $permissions['edit terms in feds_club_categories'] = array(
    'name' => 'edit terms in feds_club_categories',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter feds_club revision log entry'.
  $permissions['enter feds_club revision log entry'] = array(
    'name' => 'enter feds_club revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_club authored by option'.
  $permissions['override feds_club authored by option'] = array(
    'name' => 'override feds_club authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_club authored on option'.
  $permissions['override feds_club authored on option'] = array(
    'name' => 'override feds_club authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_club promote to front page option'.
  $permissions['override feds_club promote to front page option'] = array(
    'name' => 'override feds_club promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_club published option'.
  $permissions['override feds_club published option'] = array(
    'name' => 'override feds_club published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_club revision option'.
  $permissions['override feds_club revision option'] = array(
    'name' => 'override feds_club revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_club sticky option'.
  $permissions['override feds_club sticky option'] = array(
    'name' => 'override feds_club sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search feds_club content'.
  $permissions['search feds_club content'] = array(
    'name' => 'search feds_club content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
