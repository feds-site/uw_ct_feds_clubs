<?php

/**
 * @file
 * uw_ct_feds_clubs.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_feds_clubs_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_clubs_social_media|node|feds_club|form';
  $field_group->group_name = 'group_clubs_social_media';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'feds_club';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Social Media',
    'weight' => '6',
    'children' => array(
      0 => 'field_facebook',
      1 => 'field_twitter',
      2 => 'field_youtube',
      3 => 'field_instagram',
      4 => 'field_other',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Social Media',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-clubs-social-media field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_clubs_social_media|node|feds_club|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Social Media');

  return $field_groups;
}
