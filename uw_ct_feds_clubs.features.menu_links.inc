<?php
/**
 * @file
 * uw_ct_feds_clubs.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_feds_clubs_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: navigation_feds-club:node/add/feds-club.
  $menu_links['navigation_feds-club:node/add/feds-club'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/feds-club',
    'router_path' => 'node/add/feds-club',
    'link_title' => 'Feds club',
    'options' => array(
      'attributes' => array(
        'title' => 'Feds club listing details and contact information.',
      ),
      'identifier' => 'navigation_feds-club:node/add/feds-club',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
    'parent_identifier' => 'navigation_add-content:node/add',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Feds club');

  return $menu_links;
}
