<?php

/**
 * @file
 * uw_ct_feds_clubs.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_feds_clubs_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_feds_clubs_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_menu_default_menu_links_alter().
 */
function uw_ct_feds_clubs_menu_default_menu_links_alter(&$data) {
  if (isset($data['main-menu_feds-club-listing:clubs/listing'])) {
    $data['main-menu_feds-club-listing:clubs/listing']['hidden'] = 0; /* WAS: 1 */
    $data['main-menu_feds-club-listing:clubs/listing']['weight'] = -27; /* WAS: -29 */
  }
}

/**
 * Implements hook_node_info().
 */
function uw_ct_feds_clubs_node_info() {
  $items = array(
    'feds_club' => array(
      'name' => t('Feds club'),
      'base' => 'node_content',
      'description' => t('Feds club listing details and contact information.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
