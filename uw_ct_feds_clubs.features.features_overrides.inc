<?php

/**
 * @file
 * uw_ct_feds_clubs.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_feds_clubs_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: menu_links
  $overrides["menu_links.main-menu_feds-club-listing:clubs/listing.hidden"] = 0;
  $overrides["menu_links.main-menu_feds-club-listing:clubs/listing.weight"] = -27;

 return $overrides;
}
