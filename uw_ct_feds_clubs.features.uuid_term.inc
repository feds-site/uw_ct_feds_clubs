<?php

/**
 * @file
 * uw_ct_feds_clubs.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function uw_ct_feds_clubs_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Environmental and Sustainability',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 6,
    'uuid' => '12e787d0-85ae-4899-88e3-86d3f5ff9924',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_club_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Religious and Spiritual',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 11,
    'uuid' => '365c6858-fcca-48d1-b1ed-47edc47233a6',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_club_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Games, Recreational and Social',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 7,
    'uuid' => '6075e430-ce8f-40c3-af3d-bed04ebec836',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_club_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Health Promotion',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 8,
    'uuid' => '71f816c1-1ed3-445b-96b0-86f9456f9ef6',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_club_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Cultural',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 5,
    'uuid' => '7ee27a61-fb45-492c-92b9-c16cc5501eca',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_club_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Media, Publications and Web Development',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 9,
    'uuid' => '870e07a6-4add-4d79-9f72-0833de73022d',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_club_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Creative Arts, Dance and Music',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 4,
    'uuid' => '8b49ccab-db66-4c15-94a4-87cd2604b8ef',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_club_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Charitable, Community Service & International Development',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 3,
    'uuid' => '9c411614-b25a-4070-a3dc-9120bb119f56',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_club_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Political and Social Awareness',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 10,
    'uuid' => 'b249f724-c39a-4b48-887f-68f916cd0422',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_club_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(),
  );
  $terms[] = array(
    'name' => 'Business and Entrepreneurial',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 2,
    'uuid' => 'd8f75f72-5a1f-4d6e-b48e-58b5162941a5',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_club_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-club-categories/business-and-entrepreneurial',
        'language' => 'und',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Academic',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 1,
    'uuid' => 'db7bfaf9-f2ca-4072-889f-8d6028b9d239',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'feds_club_categories',
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
    'path' => array(
      'pathauto' => 0,
    ),
    'url_alias' => array(
      0 => array(
        'alias' => 'feds-club-categories/academic',
        'language' => 'und',
      ),
    ),
  );
  return $terms;
}
